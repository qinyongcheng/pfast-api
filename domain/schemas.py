from typing import Optional,List
from pydantic import BaseModel, constr, validator
from common.domain import Id,RoleType
from common.codegen import sqlalchemy_to_pydantic
from domain.entities import *
import re
from common.security import *

#https://blog.csdn.net/qq_33801641/article/details/120320775
#https://blog.csdn.net/qq_33801641/article/details/120320775
'''本文件为codegen自动生成，请谨慎更改，再次自动生成的时候会覆盖此文件内容'''
def is_valid_pswd(v):
    '''验证字符串是否满足 密码格式：必须包含大小写字母及数字,且长度在[6-20]位'''
    if len(v) < 6 or len(v) > 20:
        return False
    if not re.search("[a-z]", v):
        return False
    if not re.search("[A-Z]", v):
        return False
    if not re.search("\d", v):
        return False
    return True

def password_validator(cls, v, values, **kwargs):   
    #encode_password=True if 'encode_password' not in kwargs.keys() else kwargs['encode_password']  #如果不存在这个关键字则表示编码
    '''
    密码验证器：必须包含大小写字母及数字,且长度在[6-20]位
    :param v 待验证的字符串
    :param no_encode_password 是否对字符串进行加密编码返回，登录注册的时候需要返回原始密码，在增加的时候需要进行加密后保存到数据库
    '''
    if not is_valid_pswd(v):
        raise ValueError("密码错误,必须包含大小写字母及数字,且长度在[6-20]位")
    else:
        return v #if no_encode_password  else auth2Helper.encode_password(v)

class LoginUser(BaseModel):
    username: constr(min_length=6) # 无默认值，必填字段   # 有默认值，选填字段  Optional[datetime] = None  # 选填字段
    password: str
   
class RegisterUser(LoginUser):
    email:constr(max_length=30)
    role: str=RoleType.user
    @validator("password")
    def password_rule(cls, v, values, **kwargs):   
         return password_validator(cls,v,values=values,encode_password=False)

class ChangePassword(BaseModel):
    old_password: str
    new_password: str
    @validator('new_password')
    def password_rule(cls, v, values, **kwargs):   
         return password_validator(cls,v,values=values,encode_password=False)

class SendEmail(BaseModel):
    '''
    发送邮件模型
    - <br>to接收人邮件地址</br>
    - <br>subject主题，邮件标题</br>
    - <br>contents 正文内容</br>
    - <br>attachments  附件</br>
    - <br>cc 抄送人</br>
    '''
    to:Union[str,List]
    subject:str
    contents:str
    attachments:Optional[Union[str,List]]=None
    cc:Optional[Union[str,List]]=None

def password_encode(cls, v, values, **kwargs):   
    return authHelper.encode_password(v)

add_user_validators = {
    'password_validator':
    validator('password',check_fields=False)(password_encode)
}

list_excludes=['password'] 
add_excludes=['id','sys','added_time','updated_time','userid'] 
update_excludes=['sys','added_time','updated_time','password','userid'] 

ListArea= sqlalchemy_to_pydantic(Area,exclude=list_excludes,prefix='List')
AddArea= sqlalchemy_to_pydantic(Area,exclude=add_excludes,prefix='Add' ) 
UpdateArea= sqlalchemy_to_pydantic(Area,exclude=update_excludes,prefix='Update' )

ListUser= sqlalchemy_to_pydantic(User,exclude=list_excludes,prefix='List')
AddUser= sqlalchemy_to_pydantic(User,exclude=add_excludes,prefix='Add' ,validators=add_user_validators) 
UpdateUser= sqlalchemy_to_pydantic(User,exclude=update_excludes,prefix='Update' ,validators=add_user_validators)

ListAuthority= sqlalchemy_to_pydantic(Authority,exclude=list_excludes,prefix='List')
AddAuthority= sqlalchemy_to_pydantic(Authority,exclude=add_excludes,prefix='Add' ) 
UpdateAuthority= sqlalchemy_to_pydantic(Authority,exclude=update_excludes,prefix='Update' )

ListRes= sqlalchemy_to_pydantic(Res,exclude=list_excludes,prefix='List')
AddRes= sqlalchemy_to_pydantic(Res,exclude=add_excludes,prefix='Add' ) 
UpdateRes= sqlalchemy_to_pydantic(Res,exclude=update_excludes,prefix='Update' )

ListAuthorityRes= sqlalchemy_to_pydantic(AuthorityRes,exclude=list_excludes,prefix='List')
AddAuthorityRes= sqlalchemy_to_pydantic(AuthorityRes,exclude=add_excludes,prefix='Add' ) 
UpdateAuthorityRes= sqlalchemy_to_pydantic(AuthorityRes,exclude=update_excludes,prefix='Update' )

ListRole= sqlalchemy_to_pydantic(Role,exclude=list_excludes,prefix='List')
AddRole= sqlalchemy_to_pydantic(Role,exclude=add_excludes,prefix='Add' ) 
UpdateRole= sqlalchemy_to_pydantic(Role,exclude=update_excludes,prefix='Update' )

ListRoleAuthority= sqlalchemy_to_pydantic(RoleAuthority,exclude=list_excludes,prefix='List')
AddRoleAuthority= sqlalchemy_to_pydantic(RoleAuthority,exclude=add_excludes,prefix='Add' ) 
UpdateRoleAuthority= sqlalchemy_to_pydantic(RoleAuthority,exclude=update_excludes,prefix='Update' )

ListImage= sqlalchemy_to_pydantic(Image,exclude=list_excludes,prefix='List')
AddImage= sqlalchemy_to_pydantic(Image,exclude=add_excludes,prefix='Add' ) 
UpdateImage= sqlalchemy_to_pydantic(Image,exclude=update_excludes,prefix='Update' )

ListMap= sqlalchemy_to_pydantic(Map,exclude=list_excludes,prefix='List')
AddMap= sqlalchemy_to_pydantic(Map,exclude=add_excludes,prefix='Add' ) 
UpdateMap= sqlalchemy_to_pydantic(Map,exclude=update_excludes,prefix='Update' )

ListTrigger= sqlalchemy_to_pydantic(Trigger,exclude=list_excludes,prefix='List')
AddTrigger= sqlalchemy_to_pydantic(Trigger,exclude=add_excludes,prefix='Add' ) 
UpdateTrigger= sqlalchemy_to_pydantic(Trigger,exclude=update_excludes,prefix='Update' )

ListRecord= sqlalchemy_to_pydantic(Record,exclude=list_excludes,prefix='List')
AddRecord= sqlalchemy_to_pydantic(Record,exclude=add_excludes,prefix='Add' ) 
UpdateRecord= sqlalchemy_to_pydantic(Record,exclude=update_excludes,prefix='Update' )