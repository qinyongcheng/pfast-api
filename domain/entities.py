import asyncio
from datetime import datetime 
import json
from operator import index
from pydantic import BaseModel
from common.basedao import BaseEntity,init_tables,Dao
from common.domain import Id, IdEntity, Person, Result
from sqlalchemy import Column, Integer, String,Boolean, ForeignKey
from sqlalchemy.orm import Session, relationship
import enum
import re

from common.query import Where
#https://blog.csdn.net/stone0823/article/details/112344065
# 创建数据库模型（定义表结构:表名称，字段名称以及字段类型）
class User(BaseEntity,IdEntity,Person): 
    '''用户'''
    # 定义表名
    __tablename__ = 'user'
    # 定义字段
    userid=Column(Integer)#  添加此记录的用户ID，系统自动赋值，外键

class UserIdEntity(IdEntity) :
    __abstract__ = True
    userid=Column(Integer)#  添加此记录的用户ID，系统自动赋值，外键
    # User User  添加此记录的用户对象，客户端做增加修改时不需要给此属性赋值（只需要传递修改UserId属性值）

class Area(BaseEntity,UserIdEntity):
    '''区域'''
    __tablename__ = 'area'
    name =Column(String(30),nullable=False,index=True) #名称
    parentid=  Column(Integer, ForeignKey('area.id'))
    #parent = relationship("Area", back_populates="children")
    #children = relationship("Area", back_populates="parent")
    level =Column(Integer) #层级
    expanded =Column(Boolean) #是否展开
    common =Column(Boolean) #设为常用

class Authority(BaseEntity,UserIdEntity) :
    '''权限（将多种资源组合成权限）'''
    __tablename__ = 'authority'
    name =Column(String(30),nullable=False,index=True)# 权限名称,用于界面显示
    code =Column(String(30),nullable=False,index=True)# 权限标识，用于程序内部区别查找
    # 权限对应的资源（一个权限有多重资源，一个资源可以被多个权限拥有）  一个资源对应一个权限？或者一个资源可以对应多个权限？哪种更优？  
    # 。在做增加修改的的时候不需要给此属性赋值     
    #public virtual ICollection<AuthorityRes> Reses =Column(String(20)) = new HashSet<AuthorityRes>();
    intro =Column(String(512))# 简介

class Res(BaseEntity,UserIdEntity) : 
    '''资源（资源包括如api接口访问代码）'''
    __tablename__ = 'res'
    name =Column(String(30),nullable=False,index=True)#资源名称，便于人看
    value =Column(String(30),nullable=False,index=True)   # 资源值 ,如网址、方法名
    intro =Column(String(512))#资源简介
    
class AuthorityRes(BaseEntity,UserIdEntity) :#https://www.jianshu.com/p/ae313177b9f4  https://docs.sqlalchemy.org/en/13/orm/basic_relationships.html#one-to-many
    '''权限资源，即权限对应（拥有）的资源'''
    __tablename__ = 'authority_res'
    authorityid =Column(Integer, ForeignKey('authority.id'))# 权限id 可以设为外键
    authority = relationship("Authority")
    # 权限对象。客户端做增加修改时不需要给此属性赋值（修改对应的authorityId即可）
    #public virtual Authority Authority =Column(String(20))
    resid =Column(Integer, ForeignKey('res.id'))# 资源id
    res = relationship("Res")
    # 资源对象。客户端做增加修改时不需要给此属性赋值（修改对应的resId即可）
    #public virtual Res Res =Column(String(20))

class Role(BaseEntity,UserIdEntity) : 
    '''角色'''
    __tablename__ = 'role'
    name=Column(String(30),nullable=False,index=True)#角色名称
    code=Column(String(30),nullable=False,index=True)#  角色码，角色标识，用于程序内部区别查找
    #角色拥有的权限，一个角色可以拥有多个权限，一个权限可以属于多个角色，多对多关系。在做增加修改的的时候不需要给此属性赋值
    #public virtual ICollection<RoleAuthority> Authorities=Column(Integer,nullable=False,index=True) = new HashSet<RoleAuthority>();
    intro=Column(String(512)) #简介  

class RoleAuthority(BaseEntity,UserIdEntity) :  
    '''角色权限，即角色对应（拥有）的权限'''
    __tablename__ = 'role_authority'
    roleid =Column(Integer, ForeignKey('role.id'))#对应角色id
    role = relationship("Role")
    #对应角色对象， 客户端做增加修改时不需要给此属性赋值（只需要修改roleId属性值）
    #public virtual Role Role =Column(Integer,nullable=False,index=True)
    #对应权限id
    authorityid =Column(Integer, ForeignKey('authority.id'))# 权限id 可以设为外键
    authority = relationship("Authority")
    #对应权限对象， 客户端做增加修改时不需要给此属性赋值（只需要修改authorityId属性值）
    #public virtual Authority Authority =Column(Integer,nullable=False,index=True)            

class Image(BaseEntity,UserIdEntity) :
    '''图片（文档、视频）资源'''
    __tablename__ = 'image'
    title =Column(String(256))#文件在数据库中的显现名称.为空则显示为名字
    name =Column(String((256)))#文件保存在磁盘的真实名称，目前不可更改
    path =Column(String(512),nullable=False)#路径
    size =Column(Integer)#SIEZ为oracle的关键词 

'''下面自定义实体类'''

init_tables()

'''
dao=Dao(User)
o=User(username="在1")
dao.add(o)

o=User(username="在2")
o2=User(username="的")
s=[o,o2]
dao.add_all(s)
'''
'''
dao=Dao(User)
#批量添加
o=User(username="615")
o2=User(username="6415")
s=[o,o2]
dao.add_all(s)
'''


'''
dao=Dao(User)
dao.delete(2)

'''

'''
from pydantic import BaseModel
class AlterUser(Id):
    username: str

dao=Dao(User)
o=AlterUser(id=2,username="这是4444")
print(str(o.id))
dao.update(o)
'''


'''
dao=Dao(User)
dao.update_by_dict(1,{"username":"张三"})
'''

'''

dao=Dao(User)
o=dao.select_one(1)

print(str(o.data))
'''

'''

dao=Dao(User)
o=dao.select_one(conditions="username=:user or email=:email", condition_params={
                         "user": "girg123456", "email": "girg6"})

print(str(o.data.username))
print(o.status())
'''
'''

dao=Dao()
o=dao.select_all(User)
print(len(o.data))
'''
'''

o=dao.select_page(User,1,10)

print(o.data.pages)
'''

'''
dao=Dao(User)
#o=dao.select_all()
#o=dao.select_all(conditions="id<3")   #o=dao.select_all("id<3")
#o=dao.select_all(conditions="id<:id",condition_params={"id":3})  #o=dao.select_all("id<:id",{"id":3})
#print(len(o.data))
'''
'''

dao=Dao(User)
params = []
params.append(User.id < 4)
o=dao.select_all(condition_list=params)
print(len(o.data))

'''
'''
dao=Dao(User)
params = []
params.append(User.id <= 4)
r=dao.select_page(1,3,condition_list=params)
print(r.data.pages)
'''
'''
dao=Dao(User)
p=Pageparam(page_no=1,page_size=2)
r=dao.select_page_by_pageparam(p)
#print(r.data.pages)

print(json.dumps(r))
'''
'''

import ast

v={"id":1,"u":"eee"}
print(str(v))

ast.literal_eval(str(v))
'''


'''
r=Result(msg="ddd",data={"u":"rrrrrrrrrr"})

print(r.data)

'''

if __name__ == '__main__':
    import json 
    def isdigit(str):
        return re.match("^[-+]?[0-9]+$",str) is not  None
    def isfloat(str):
        return re.match("^([-+]?[0-9]+)(\.\d+)$",str) is not  None
    def isdate(str):
        return re.match("^\d{4}-\d{1,2}-\d{1,2}$",str) is not  None
    def istime(str):
        return re.match("^(20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d$",str) is not  None
    def isdatetime(str):
        return re.match("^\d{4}-\d{1,2}-\d{1,2} (20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d$",str) is not  None
    params = []
    condition_text=[]
    condition_params={}
    ops={"eq":"=","lt":"<","ne":"!="}
    f={"id__ne":"3"}
    for ks in f.keys():
        key_array=ks.split("__")
        op="="
        k=key_array[0]
        if len(key_array)==2:
            op=key_array[1]
            op=ops[op] if ops[op] else op
        #print(datetime.strptime('2015-10-28', '%Y-%m-%d'))
        #print(type(getattr(User,e)))
        #params.append(getattr(User,e) < int("4"))
        if hasattr(User,k):
            v_str=f[ks]
            if isfloat(v_str):
                v=float(v_str)
            elif isdigit(v_str):
                v=int(v_str)    
            elif isdate(v_str):
                v=datetime.strptime(v_str, '%Y-%m-%d')
            elif isdatetime(v_str):
                v=datetime.strptime(v_str, '%Y-%m-%d %H:%M:%S')
            else:
                v=v_str
            
            condition_text.append(k+" "+op+" :"+k)
            condition_params[k]=v

            #params.append(getattr(User,k) ==v)
        

    c=" and ".join(condition_text)
    print(c)
    print(condition_params)
   # w= Where(User,condition_list=params)
    w= Where(User,conditions=c,condition_params=condition_params)
   
    dao=Dao(User)
    o=dao.select_all(where=w)
    d= o.data
    print(d)
    
    #print(isdatetime("2021-2-01 15:21:12"))

    #o=asyncio.run(dao.select_all(condition_list=params))
    #print(len(o.data))
    