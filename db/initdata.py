#  -*-coding:utf8 -*-
import asyncio
from common.basedao import Dao
from domain.entities import *
from common.security import authHelper
from common.domain import *

'''初始化数据'''
'''本文件为codegen自动生成，请谨慎更改，再次自动生成的时候会覆盖此文件内容'''
'''#https://blog.csdn.net/bh57599/article/details/100948050'''
resdao=Dao(Res)
ress=[
Res(name='获取用户信息',value='userinfo'),
Res(name='区域列表查询',value='area_list'),Res(name='区域详情查询',value='area_get'),Res(name='区域增加',value='area_add'),Res(name='区域修改',value='area_update'),Res(name='区域删除',value='area_delete'),Res(name='用户列表查询',value='user_list'),Res(name='用户详情查询',value='user_get'),Res(name='用户增加',value='user_add'),Res(name='用户修改',value='user_update'),Res(name='用户删除',value='user_delete'),Res(name='权限列表查询',value='authority_list'),Res(name='权限详情查询',value='authority_get'),Res(name='权限增加',value='authority_add'),Res(name='权限修改',value='authority_update'),Res(name='权限删除',value='authority_delete'),Res(name='资源列表查询',value='res_list'),Res(name='资源详情查询',value='res_get'),Res(name='资源增加',value='res_add'),Res(name='资源修改',value='res_update'),Res(name='资源删除',value='res_delete'),Res(name='权限资源列表查询',value='authorityres_list'),Res(name='权限资源详情查询',value='authorityres_get'),Res(name='权限资源增加',value='authorityres_add'),Res(name='权限资源修改',value='authorityres_update'),Res(name='权限资源删除',value='authorityres_delete'),Res(name='角色列表查询',value='role_list'),Res(name='角色详情查询',value='role_get'),Res(name='角色增加',value='role_add'),Res(name='角色修改',value='role_update'),Res(name='角色删除',value='role_delete'),Res(name='角色权限列表查询',value='roleauthority_list'),Res(name='角色权限详情查询',value='roleauthority_get'),Res(name='角色权限增加',value='roleauthority_add'),Res(name='角色权限修改',value='roleauthority_update'),Res(name='角色权限删除',value='roleauthority_delete'),Res(name='图片列表查询',value='image_list'),Res(name='图片详情查询',value='image_get'),Res(name='图片增加',value='image_add'),Res(name='图片修改',value='image_update'),Res(name='图片删除',value='image_delete'),Res(name='示意图列表查询',value='map_list'),Res(name='示意图详情查询',value='map_get'),Res(name='示意图增加',value='map_add'),Res(name='示意图修改',value='map_update'),Res(name='示意图删除',value='map_delete'),Res(name='触发器列表查询',value='trigger_list'),Res(name='触发器详情查询',value='trigger_get'),Res(name='触发器增加',value='trigger_add'),Res(name='触发器修改',value='trigger_update'),Res(name='触发器删除',value='trigger_delete'),Res(name='历史记录列表查询',value='record_list'),Res(name='历史记录详情查询',value='record_get'),Res(name='历史记录增加',value='record_add'),Res(name='历史记录修改',value='record_update'),Res(name='历史记录删除',value='record_delete'),
]
asyncio.run(resdao.add_all(ress))
#初始管理员
userdao=Dao(User)
user=User(username="admin",role=RoleType.admin,password=authHelper.encode_password("123456"))
asyncio.run(userdao.add(user))