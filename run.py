'''
import sys,os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # __file__获取执行文件相对路径，整行为取上一级的上一级目录
sys.path.append(BASE_DIR)
#在系统环境变量设置https://cloud.tencent.com/developer/article/1473765
'''
import uvicorn as uvicorn  # 运行服务器
from fastapi import FastAPI, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.exceptions import RequestValidationError
from fastapi.encoders import jsonable_encoder
from common.domain import Result
from fastapi.responses import JSONResponse
from router import routers
from router.test import router as test_router
from router.default import router as default_router

# 类似于 app = Flask(__name__)
app = FastAPI(title="环境监测系统API",
              description="环境监测系统API文档",
              root_path="/",    #/pai/ 部署到服务器反向代理路径问题  https://www.jianshu.com/p/3a8675ee2285
              docs_url="/", version='0.0.1')  # 必须实例化该类，启动的时候调用   docs_url="/docs"
# 跨域支持 https://zhuanlan.zhihu.com/p/397029492
app.add_middleware(CORSMiddleware,
                   allow_origins=['*'],
                   allow_credentials=True,
                   allow_methods=['*'],
                   allow_headers=['*'])

# 自定义请求验证异常处理，统一返回数据格式
@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc: RequestValidationError):
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder(Result().error(code=1002, msg="请求参数验证失败:"+str(
            [i['msg'] for i in exc.errors()])))  # '''data=exc.json()'''
    )

# 绑定(注册)路由
app.include_router(test_router, tags=['常用接口'])
app.include_router(default_router, prefix="", tags=['常用接口'])
routers.register(app)
# 在 Windows 中必须加上 if __name__ == "__main__"，否则会抛出 RuntimeError: This event loop is already running
if __name__ == '__main__':
    # 启动服务，因为我们这个文件叫做 main.py，所以需要启动 main.py 里面的 app
    # 第一个参数 "main:app" 就表示这个含义，然后是 host 和 port 表示监听的 ip 和端口
    uvicorn.run(app=app, host="127.0.0.1", port=8000, debug=True)
    # 生产环境运行：uvicorn main:app --port 11341 --reload
