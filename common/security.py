from typing import Union
from typing import Optional, Dict
from jose import JWTError, jwt  # used for encoding and decoding jwt tokens
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm, OAuth2AuthorizationCodeBearer
from starlette.requests import Request
# python密码加密算法库 # used for hashing the password
from passlib.context import CryptContext
# used to handle expiry time for tokens
from datetime import datetime, timedelta
import json 
from common.domain import  RoleType, UserInfo
from common.toolkit import config
# https://dev.to/deta/get-started-with-fastapi-jwt-authentication-part-2-18ok
# https://mbd.baidu.com/ug_share/mbox/4a81af9963/share?tk=83ee4583bd08677f72c11d12c1fc22a4&share_url=https%3A%2F%2Fwjrsbu.smartapps.cn%2Fzhihu%2Farticle%3Fid%3D388155597%26isShared%3D1%26_swebfr%3D1%26_swebFromHost%3Dbaiduboxapp
# https://www.cnblogs.com/leiziv5/p/15416764.html

# https://www.cnblogs.com/gcxblogs/p/14774897.html

#认证
class AuthHelper():
    '''
    认证帮助类
    '''
    def __init__(self, secret='@abc123@abc321@',  algorithm="HS256") -> None:
        '''
        :param  algorithm  加密算法，默认为"HS256"
        '''
        #self.auth2 = OAuth2PasswordBearer(tokenUrl=tokenUrl)
        # 通过passlib中CryptContext实现加密，解密
        self.password_context = CryptContext(
            schemes=['bcrypt'], deprecated='auto')  # schemes指定加密方式   schemes=["sha512_crypt"]
        self.secret = secret
        self.algorithm = algorithm

    def encode_password(self, password):
        '''
        获取密码,工具函数以哈希来自用户的密码
        :param password 原始密碼
        :return 返回加密后的密碼
        '''
        return self.password_context.hash(password)

    def verify_password(self, plain_password, encoded_password):
        '''
        验证密码,工具函数，用于校验接收的密码是否与存储的哈希值匹配。
        :param plain_password 原始密碼
        :param encoded_password 加密后的密碼
        '''
        return self.password_context.verify(plain_password, encoded_password)

    def encode_token(self,
                     data: Union[UserInfo,str],
                     expires_delta: Optional[timedelta] = None):
        '''
        生成新的访问令牌的工具函数
        :param data 包含用户名、邮箱等的参数数据。只能传UserInfo类型，str是作为内部使用
        :param expires_delta 过期时间,，如果未传使用默认过期时间60*24*7分钟
        :return 生成access_token，用来访问api，有效期较短
        '''
        #to_encode = data.copy()
        ## 如果传入了过期时间, 那么就是用该时间, 否则使用默认的时间
        expire = datetime.utcnow() + expires_delta if expires_delta else datetime.utcnow() + \
            timedelta(minutes=60*24*7)
        #payload = {'exp': expire, 'iat': datetime.utcnow(), 'sub': username}
        # 需要加密的数据data必须为一个字典类型, 在数据中添加过期时间键值对, 键exp的名称是固定写法
        to_encode = {"exp": expire, 'iat': datetime.utcnow(
        ), 'scope': 'access_token', "sub": data if isinstance(data,str) else json.dumps(data.dict())}
        # 进行jwt加密
        return jwt.encode(to_encode, self.secret, algorithm=self.algorithm)

    def decode_token(self, token)->UserInfo:
        '''
        token解码
        '''
        try:
            payload = jwt.decode(token, self.secret,
                                 algorithms=[self.algorithm])
            if (payload['scope'] == 'access_token'):  
                data=json.loads(payload["sub"])
                return UserInfo(**data)
            raise HTTPException(
                status_code=401, detail='Scope for the token is invalid,expect access_token value')
        except jwt.ExpiredSignatureError:
            raise HTTPException(status_code=401,
                                detail='access_token has expired')
        except JWTError as e:
            print(str(e))
            raise HTTPException(status_code=401, detail='Invalid token')

    def encode_refresh_token(self,
                             data: UserInfo,
                             expires_delta: Optional[timedelta] = None):
        '''
        生成刷新访问令牌的工具函数
        :param data 包含用户名、邮箱等的参数数据。只能传UserInfo类型，str是作为内部使用
        :param expires_delta 过期时间,，如果未传使用默认过期时间60*24*365*10分钟,即永久
        :return 生成refresh_token，refresh_token用来生成access_token，有效期较长，过期后需重新登录,重新登录就用refresh_token来生成
        '''
        #to_encode = data.copy()
        ## 如果传入了过期时间, 那么就是用该时间, 否则使用默认的时间
        expire = datetime.utcnow() + expires_delta if expires_delta else datetime.utcnow() + \
            timedelta(minutes=60*24*365*10)
        #payload = {'exp': expire, 'iat': datetime.utcnow(), 'sub': username}
        # 需要加密的数据data必须为一个字典类型, 在数据中添加过期时间键值对, 键exp的名称是固定写法
        to_encode = {"exp": expire, 'iat': datetime.utcnow(
        ), 'scope': 'refresh_token', "sub": data if isinstance(data,str) else json.dumps(data.dict())}
        # 进行jwt加密
        return jwt.encode(to_encode, self.secret, algorithm=self.algorithm)

    def refresh_token(self, refresh_token, expires_delta: Optional[timedelta] = None):
        '''
        根据刷新token即refresh_token生成心的token
        :param refresh_token 刷新token
        :return 返回新的token
        '''
        try:
            payload = jwt.decode(refresh_token, self.secret,
                                 algorithms=[self.algorithm])
            if (payload['scope'] == 'refresh_token'):
                data = payload['sub']
                new_token = self.encode_token(data, expires_delta)
                return new_token
            raise HTTPException(
                status_code=401, detail='Invalid scope for token,expect refresh_token value')
        except jwt.ExpiredSignatureError:
            raise HTTPException(
                status_code=401, detail='refresh_token expired')
        except jwt.InvalidTokenError:
            raise HTTPException(
                status_code=401, detail='Invalid refresh token')

 
authHelper = AuthHelper(secret=config.get("security")["secret"])
AuthForm = OAuth2PasswordRequestForm
# https://blog.csdn.net/vanexph/article/details/105030757

# 授权
class Authorize(OAuth2PasswordBearer):
    '''
    授权类，依赖注入tag
    '''

    def __init__(self, res_cod: Optional[str] = None,role: Optional[str] = None, tokenUrl='token', scheme_name: Optional[str] = None,
                 scopes: Optional[Dict[str, str]] = None,
                 description: Optional[str] = None,
                 auto_error: bool = True):
        '''
        :param res_code 资源代码
        :param role 角色代码，此资源需要的角色，如果传递了角色，直接判断会员是否拥有此角色，不再获取用户对应的角色，根据角色查权限，根据权限查资源。
        :tokenUrl param: tokenUrl 这个参数的作用是当输入完用户密码后, 点击Authorize按钮后, 会请求127.0.0.1:8000/xxx这个网址, 把用户密码通过Form表单的形式传递给这个请求, 即这个/xxx就是我们网站的后台登录接口.
        '''
        super().__init__(tokenUrl, scheme_name, scopes, description, auto_error)
        self.res_code = res_cod
        self.role=role

    async def __call__(self,  request: Request)->UserInfo:
        token = await super().__call__(request=request)
        data=authHelper.decode_token(token=token)
        if data.role!=RoleType.admin:#管理员拥有全部权限，否则则判断角色是否拥有对应的资源
            if self.role is not None and self.role in data.role: #如果此资源明确了角色，且该角色在会员拥有的角色中，表示有权限直接返回
                return data
            else:
                pass
                '''
                此处可以判断用户是否有对应资源的权限（根据数据库查询）  待实现
                '''
        return data


if __name__ == '__main__':
    a = AuthHelper()
    print(a.encode_token("fggg"))
    #print(a.decode_token("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDk4NjMxNzIsImlhdCI6MTY0OTI1ODM3Miwic2NvcGUiOiJhY2Nlc3NfdG9rZW4iLCJzdWIiOiJmZ2dnIn0.nOlTF_Y6WxTX6G9cqfLE87y792sxFKF676OuZrFwYVs"))
    print(a.encode_password("123ABCabc"))
