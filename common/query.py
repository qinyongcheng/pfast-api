from email.policy import default
from typing import List, Optional,TypeVar, Generic,Dict
from sqlalchemy import Column, Identity, Integer, String,Boolean,func,DateTime,Enum,Float,Numeric,TIMESTAMP
from sqlalchemy.orm import Session, relationship
from sqlalchemy.sql import func
from pydantic.generics import GenericModel
import math
from common.toolkit import *
from starlette.requests import Request
from fastapi import Depends, HTTPException
from common.security import Authorize
from common.domain import IdEntity, RoleType,T

class Pageparam(GenericModel):
    '''
    接收分页查询参数，用于封装接收客户端传递过来的分页信息。page_no 页码，从1开始，默认1； page_siz每页显示数量，默认12。where查询条件字符串
    '''  
    page_no :int =1
    page_size:int=12
#分页对象
class Pageobj(Pageparam, Generic[T]):
    '''
    分页对象信息,用于封装返回给客户端的分页数据。page_no 页码，从1开始，默认1； page_siz每页显示数量，默认12
    '''  
    items:List[T]#分页查询数据
    counts:int=0#总数据量
    pages:int=0#总页数
    next_page_no:int=None#下一页
    prev_page_no:int=None#上一页
   

#分页类
class Pagination():
    '''分页执行类，用于根据参数和query查询对象在数据库执行分页'''
    def __init__(self, query, page_no: int = 1, page_size: int = 30,db:Session=None,type:IdEntity=None):
        """
        初始化分页参数
        :param query: 查询对象
        :param page_size: 一页多少内容
        :param page_no: 第几页 1起
        """
        self.query = query
        self.page_size = page_size
        self.page_no = page_no
        self.db=db
        self.type=type

    def get_pageobj(self):
        return Pageobj(page_no=self.page_no,page_size=self.page_size,items=self.items,counts=self.counts,pages=self.pages,next_page_no=self.next_page_no,prev_page_no=self.prev_page_no)

    @property
    def items(self):
        """
        得到分页后的内容
        :return: [model row / Model]
        """
        if self.page_no > self.pages:
            return []
        offset_num = (self.page_no-1) * self.page_size
        return self.query.limit(self.page_size).offset(offset_num).all()

    @property
    def counts(self):
        """
        总数据量
        :return: int
        """
        #return self.query.count() if self.db is None or self.type is None else self.db.query(func.count(self.type.id)).scalar()
        return self.query.count() 

    @property
    def pages(self):
        """
        总页数
        :return: int
        """
        return  math.ceil(self.counts / self.page_size)

    @property
    def next_page_no(self):
        """下一页"""
        next_page_no = self.page_no + 1
        if self.pages <next_page_no:
            return None
        return next_page_no

    @property
    def prev_page_no(self):
        """上一页"""
        prev_page_no = self.page_no - 1
        if prev_page_no < 1:
            return None
        return prev_page_no

    '''
    def iter_pages(self, left=2, right=2):
        """显示在前端的页码列表"""
        length = left + right + 1
        # 页数大于
        if self.page_no > self.pages:
            range_start = self.pages - length
            if range_start <= 0:
                range_start = 1
            return range(range_start, self.pages + 1)

        # 页数小于最少分页数
        if self.pages < length:
            return range(1, self.pages + 1)

        # 页数正常的情况下,至少大于 length 长度
        l_boundary, r_boundary = left + 1, self.pages - right + 1
        if l_boundary < self.page_no < r_boundary:
            return range(self.page_no - left, self.page_no + right + 1)
        if self.page_no <= left:
            return range(1, length + 1)
        return range(self.pages - length, self.pages + 1)
    '''

class Where():
    ops={"eq":"=","ne":"!=","lt":"<","lte":"<=","gt":">","gte":">=","in":" in "}  #对应客户端传递过来的操作代码解析
    '''
    查询条件构建类，从客户端查询字符串接收查询参数并解析。
    条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 
    即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，
    大小写保存一致条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。
    如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”

    :param type 如果要解析查询字符串，此参数必填
    '''
    def __init__(self, type:Optional[Identity] = None,condition_list:Optional[List]=None,conditions:Optional[str]=None,condition_params:Optional[dict]=None):
        self.type=type
        self.condition_list=condition_list  #列表形式的条件
        self.conditions=conditions   #字符串形式的条件
        self.condition_params=condition_params  #字典形式的条件的参数值
    '''
    解析客户端传递过来的条件查询字符串
    '''
    async def __call__(self,  request: Request):#http://www.04007.cn/article/924.html
        if self.type is None:
            return
        condition_text=[]
        condition_params={}
        for key in request.query_params:
            key_array=key.split("__")   
            op="="
            k=key_array[0]
            if len(key_array)==2:
                op=key_array[1]
                op=self.ops[op] if self.ops[op] else op
            if hasattr(self.type,k):
                v_str=request.query_params[key]
                if isfloat(v_str):
                    v=float(v_str)
                elif isdigit(v_str):
                    v=int(v_str)    
                elif isdate(v_str):
                    v=datetime.strptime(v_str, '%Y-%m-%d')
                elif isdatetime(v_str):
                    v=datetime.strptime(v_str, '%Y-%m-%d %H:%M:%S')
                else:
                    v=v_str
                #解析操作符
                condition_text.append(k+ op +":"+k)
                condition_params[k]=v
                #self.conditions.append(getattr(self.type,k) == v)  换成用字符串 字典形式的
        self.conditions=" and ".join(condition_text)
        self.condition_params=condition_params
        return self

    def append(self, conditions:str,condition_params:Optional[dict]=None ):
        '''添加条件'''   
        if self.conditions is not None and self.conditions.strip()!="":
            self.conditions=self.conditions +" and "+conditions
        else:
            self.conditions=conditions

        if condition_params is not None:
            if self.condition_params is not  None:#在原来基础上复制
                for key in condition_params.keys():
                    self.condition_params[key] = condition_params[key]
            else:
                self.condition_params=condition_params

        return self

class Whereis(Where,Authorize):
    '''
    安全条件过滤：where is security ，即在基本查询条件Where基础上加上授权。授权类型参考RoleType：
    user（普通会员）只能查看、删除、修改自己添加的信息；
    admin（超级管理员），超级管理员拥有所有权限；
    tenant（租户）适用于多租户模式下，只能查看、删除、修改本机构添加的信息。
    '''
    def __init__(self, type:Optional[Identity] = None,condition_list:Optional[List]=None,
                 conditions:Optional[str]=None,condition_params:Optional[dict]=None,
                 res_cod: Optional[str] = None,
                 tokenUrl='token', scheme_name: Optional[str] = None,
                 scopes: Optional[Dict[str, str]] = None,
                 description: Optional[str] = None,
                 auto_error: bool = True):
        Where.__init__(self, type,condition_list=condition_list,conditions=conditions,condition_params=condition_params)
        Authorize.__init__(self,  res_cod= res_cod,tokenUrl= tokenUrl, scheme_name=scheme_name,
                scopes= scopes, description=description,auto_error= auto_error)
    '''
    解析客户端传递过来的条件查询字符串 
    '''
    async def __call__(self,  request: Request):#http://www.04007.cn/article/924.html
        self.user=user=await Authorize.__call__(self, request= request)
        await Where.__call__(self, request= request)
        if RoleType.admin in user.role:#如果是管理员 拥有所有权限
            pass
        elif RoleType.user in user.role:#普通会员添加过滤条件
            super().append(" userid=:userid",{"userid":user.id})
       
        return self
