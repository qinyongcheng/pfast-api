## 连接数据库
from fastapi import Query,Depends
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
# declarative_base类维持了一个从类到表的关系，通常一个应用使用一个Base实例，所有实体类都应该继承此类对象
from sqlalchemy.ext.declarative import declarative_base
#对MySQL数据库的支持
import pymysql
pymysql.install_as_MySQLdb()
from common.domain import Id, IdEntity,Result
from common.query import Pagination,Where,Pageparam
from typing import List, Optional,overload,Union
from sqlalchemy.sql import text
from functools import singledispatchmethod
from common.toolkit import config

# 配置数据库地址：数据库类型+数据库驱动名称://用户名:密码@机器地址:端口号/数据库名 https://zhuanlan.zhihu.com/p/387078089
DATABASE_URL_SQLITE =config.get("db")["url"] #"sqlite:///./db/pai.sqlite3" 
#connect_args 这个只有sqlite才用
#engine = create_engine(DATABASE_URL_SQLITE,  connect_args={"check_same_thread": False})
#DATABASE_URL_SQLITE = "mysql+pymysql://root:girg@114.55.26.103:3306/pai"
# 使用pymssql驱动连接到sql server
#DATABASE_URL_SQLITE = 'mssql+pymssql://user:pwd@localhost:1433/testdb'
engine = create_engine(DATABASE_URL_SQLITE,  connect_args={})
# 把当前的引擎绑定给这个会话；
# autocommit：是否自动提交 autoflush：是否自动刷新并加载数据库 bind：绑定数据库引擎
# 实例化
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)

BaseEntity = declarative_base()
#初始化数据库表
def init_tables():
    '''在数据库中生成表'''
    BaseEntity.metadata.create_all(bind=engine)

def drop_db():
    """从数据库中删除所有定义的表"""
    BaseEntity.metadata.drop_all(engine)
    
#获取session实例对象 https://www.cnblogs.com/leiziv5/p/15416805.html
#yield https://copyfuture.com/blogs-details/20210929165110708s
async def get_db():
    """
    每一个请求处理完毕后会关闭当前连接，不同的请求使用不同的连接
    :return:
    """
    db = Session()
    try:
        yield db
    finally:
        db.close()

class Dao():
    db=None ## 实例化db=session = Session()
    def __init__(self,type:IdEntity,db:Session=None):
        """
        初始化
        :param type: 查询的实体类型
        :param db: Session对象，如果为None,默认初始化一个Session实例对象
        """ 
        self.type=type
        self.db=db if db is not None else Session()

    async def __call__(self,db:Session=Depends(get_db) ):   
        '''
        作为被fastapi框架依赖注入调用方法，自动注入seeson
        '''
        self.db=db
        return self

    #https://www.bbsmax.com/A/rV57609EJP/
    #添加实体对象
    async def add(self,o:IdEntity):
        try:
            self.db.add(o)
            self.db.commit()
            self.db.refresh(o)
            return Result().success(msg="添加成功",data=o)
        except Exception as e:         
            return Result().error(code=1001,msg=f"添加失败,数据库错误,请联系管理员!{str(e)}")

    async def add_all(self,objs:List[IdEntity]):
        try:
            #objs是一个列表，每个内部元素均为实体对应类型
            self.db.add_all(objs)
            self.db.commit()
            return Result().success(msg="添加成功")
        except Exception as e:
            return Result().error(code=1001,msg=f"添加失败,数据库错误,请联系管理员!{str(e)}")

    #根据其他条件删除多个，删除满足条件的
    async def delete(self,id:Optional[int]=None,where:Optional[Where]=None):
        '''
        根据id或条件删除多个，删除满足条件的
        :param where条件解析对象
        '''
        try:
            q = self.__build_conditions(where=where)
            if id is not  None:
                q=q.filter(self.type.id ==id)   
            objs = q.all()
            for o in objs :
                self.db.delete(o)
                self.db.commit()
                self.db.flush()           
            return Result().success(msg="删除成功")
        except Exception as e:
            return Result().error(code=1001,msg=f"删除失败,数据库错误,请联系管理员!{str(e)}")
       
    #批量删除 
    async def delete_batch(self,ids: List[int],where:Optional[Where]=None):
        '''
        根据id和条件批量删除
        :param where条件解析对象
        '''
        try:
            q = self.__build_conditions(where=where)
            objs=q.filter(self.type.id in  ids).all()
            for o in objs :
                self.db.delete(o)
                self.db.commit()
                self.db.flush()           
            return Result().success(msg="删除成功")
        except Exception as e:
            return Result().error(code=1001,msg=f"删除失败,数据库错误,请联系管理员!{str(e)}")
       
    #根据id修改实体对象      
    async def update(self,source:Union[Id,IdEntity],where:Optional[Where]=None):
        """
        根据实体对象(具有id属性值)修改实体对象.
        :param source: 要修改的实体对象，有两种类型参数：一类是要求同时继承Id和BaseModel类的参数，二类是同时继承IdEntity和BaseEntity类的参数。
        其中修改第二种类型的参数，要求处于托管状态，即从数据库里查询出来基础上修改的
        :param where条件解析对象
        :returns: 返回Result
        """ 
        try:
            o=None 
            if isinstance(source,BaseEntity):#如果是非BaseEntity类型则 查询拷贝  否则直接提交
                o=source
            else:
                q = self.__build_conditions(where=where)
                o=q.filter(self.type.id ==source.id).first()
                o.copy_from(source)
            if o:
                self.db.commit()
                self.db.flush()
                self.db.refresh(o)          
                return Result().success(msg="修改成功",data=o)
            else:
                return Result().error(code=1001,msg=f"修改失败,未找到要修改的实体对象{id}")
        except Exception as e:
            return Result().error(code=1001,msg=f"修改失败,数据库错误,请联系管理员!{str(e)}")
        

    #根据id修改实体对象      
    async def update_by_dict(self,id:int,attrs:dict,where:Optional[Where]=None):
        """
        根据id修改实体对象.
        :param id: id值
        :param attrs: 要修改的属性=值对字典{name:"张三"}
        :param where条件解析对象
        :returns: 返回Result
        """ 
        try:
            q = self.__build_conditions(where=where)
            ret = q.filter(self.type.id ==id).update(attrs)# ret就是我们当前这句更新语句所更新的行数
            self.db.commit()
            self.db.flush()
            if ret>0:
                return Result().success(msg="修改成功")
            else:
                return Result().error(code=1001,msg=f"修改失败")
        except Exception as e:
            return Result().error(code=1001,msg=f"修改失败,数据库错误,请联系管理员!{str(e)}")
       

    #根据id或查询条件获取单个
    async def select_one(self,id: Optional[int]=None,conditions:Optional[str]=None,condition_params:Optional[dict]=None,condition_list:Optional[List]=None,where:Optional[Where]=None):
        """
        根据id/或其他条件查询单个.
        :param id: id值
        :param conditions: 查询条件字符串，如："id<:id and name=:name",eg:
         o=dao.select_one(conditions="id<3")   等价于 o=dao.select_one("id<3") 根据条件查询所有
        :param condition_params:查询条件参数值，对应condition里面的值，如：{"id":5},eg:
         o=dao.select_one(conditions="id<:id",condition_params={"id":3})  等价于 o=dao.select_one("id<:id",{"id":3}) 根据条件查询所有，使用参数
        :param condition_list: 查询条件列表，如：
         params = []
         params.append(User.id <= 4)
         o=select_one(params)
        :param where条件解析对象
        :returns: 返回查询的实体对象
        """ 
        try:
            q = self.__build_conditions(conditions=conditions,condition_params=condition_params,condition_list=condition_list,where=where)
            if id is not  None:
                q=q.filter(self.type.id ==id)   
            o=q.first()  
            if o:          
                return Result().success(msg="查询成功",data=o)
            else:
                return Result().error(code=1001,msg=f"查询失败,未找到要查询的实体对象")
        except Exception as e:
            return Result().error(code=1001,msg=f"查询失败,数据库错误,请联系管理员!{str(e)}")
   
    #查询所有，不分页
    def select_all(self,conditions:Optional[str]=None,condition_params:Optional[dict]=None,condition_list:Optional[List]=None,where:Optional[Where]=None)->Result:
        """
        查询满足条件的所有.eg:
        o=dao.select_all() 查询所有
        :param conditions: 查询条件字符串，如："id<:id and name=:name",eg:
         o=dao.select_all(conditions="id<3")   等价于 o=dao.select_all("id<3") 根据条件查询所有
        :param condition_params:查询条件参数值，对应condition里面的值，如：{"id":5},eg:
         o=dao.select_all(conditions="id<:id",condition_params={"id":3})  等价于 o=dao.select_all("id<:id",{"id":3}) 根据条件查询所有，使用参数
        :param condition_list: 查询条件列表，如：
         params = []
         params.append(User.id <= 4)
         o=select_all(params)
        :param where条件解析对象
        :returns: 返回查询的数据列表
        """   
        try:
            return Result().success(msg=f"查询成功",data=self.__build_conditions(conditions=conditions,condition_params=condition_params,condition_list=condition_list,where=where).all())
        except Exception as e:
            return Result().error(code=1001,msg=f"查询失败,数据库错误,请联系管理员!{str(e)}")
       
    #https://www.cnblogs.com/xushuhai/p/8846997.html
    #https://www.osgeo.cn/sqlalchemy/orm/tutorial.html#counting
    #分页查询 
    #@singledispatchmethod  
    async def select_page(self,page_no: Optional[int] = 1, page_size:Optional[int]= 20,page_param:Optional[Pageparam]=None,conditions:Optional[str]=None,condition_params:Optional[dict]=None,condition_list:Optional[List]=None,where:Optional[Where]=None):
        """
        分页查询. 
        :param page_no: 页码,默认1
        :param page_size: 每页显示记录数，默认20
        :param conditions: 查询条件字符串，如："id<:id and name=:name",eg:
         o=dao.select_page(conditions="id<3")   等价于 o=dao.select_page("id<3") 根据条件查询所有
        :param condition_params:查询条件参数值，对应condition里面的值，如：{"id":5},eg:
         o=dao.select_page(conditions="id<:id",condition_params={"id":3})  等价于 o=dao.select_page("id<:id",{"id":3}) 根据条件查询所有，使用参数
        :param condition_list: 查询条件列表，如：
         params = []
         params.append(User.id <= 4)
         o=select_page(params)
        :param where条件解析对象
        :returns: 返回查询的数据列表
        """   
        if page_param is not None:
            page_no=page_param.page_no
            page_size=page_param.page_size
        return self.__select_page(self.__build_conditions(conditions=conditions,condition_params=condition_params,condition_list=condition_list,where=where),page_no, page_size)
  
    def __select_page(self,query:Query,page_no: int = 1, page_size: int = 20):
        """
        分页查询. 
        :param query: 查询对象
        :param page_no: 页码
        :param page_size: 每页显示记录数
        :returns: 返回查询的数据列表
        """   
        try:
            return Result().success(msg=f"查询成功",data=Pagination(query=query,page_no=page_no,page_size=page_size,db=self.db,type=self.type).get_pageobj())
        except Exception as e:
            return Result().error(code=1001,msg=f"查询失败,数据库错误,请联系管理员!{str(e)}")

    def __build_conditions(self,conditions:Optional[str]=None,condition_params:Optional[dict]=None,condition_list:Optional[List]=None,where:Optional[Where]=None):
        """
        分页查询. 
        :param page_no: 页码,默认1
        :param page_size: 每页显示记录数，默认20
        :param conditions: 查询条件字符串，如："id<:id and name=:name",eg:
         o=dao.select_all(conditions="id<3")   等价于 o=dao.select_all("id<3") 根据条件查询所有
        :param condition_params:查询条件参数值，对应condition里面的值，如：{"id":5},eg:
         o=dao.select_all(conditions="id<:id",condition_params={"id":3})  等价于 o=dao.select_all("id<:id",{"id":3}) 根据条件查询所有，使用参数
        :param condition_list: 查询条件列表，如：
         params = []
         params.append(User.id <= 4)
         o=select_all(params)
        :param where条件解析对象

        :returns: 返回查询的数据列表
        """   
        q= self.db.query(self.type)
        if conditions is not  None:
            q=q.filter(text(conditions))
        if condition_params is not None:
            q=q.params(**condition_params)
        if condition_list is not None:
            q=q.filter(*condition_list)#https://www.jianshu.com/p/a33f48387efa 
        if where is not None:
            if where.condition_list is not None:
                q=q.filter(*where.condition_list)
            if where.conditions is not  None:
                q=q.filter(text(where.conditions))
            if where.condition_params is not None:
                 q=q.params(**where.condition_params)
        return q
