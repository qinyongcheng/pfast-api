from email.policy import default
from typing import List, Optional,TypeVar, Generic
from sqlalchemy import Column, Integer, String,Boolean,func,DateTime,Enum,Float,Numeric,TIMESTAMP
from sqlalchemy.orm import Session, relationship
from sqlalchemy.sql import func
from pydantic import BaseModel
from pydantic.generics import GenericModel
import math
import json
import datetime
import enum
from common.toolkit import MyJsonEncoder
from starlette.requests import Request
from fastapi import Depends, HTTPException
import sqlalchemy.types as types
from decimal import Decimal as D

class SqliteNumeric(types.TypeDecorator):#https://stackoverflow.com/questions/10355767/how-should-i-handle-decimal-in-sqlalchemy-sqlite
    impl = types.String
    def load_dialect_impl(self, dialect):
        return dialect.type_descriptor(types.VARCHAR(100))
    def process_bind_param(self, value, dialect):
        return str(value)
    def process_result_value(self, value, dialect):
        return D(value)

#Numeric = SqliteNumeric

#枚举 性别
class Gender(str, enum.Enum):
    man = "man"
    women = "women"
    unknown = 'unknown'
#枚举 角色类型
class RoleType(str, enum.Enum):
    '''
    <br>角色类型，系统内置有user（普通会员）只能查看、删除、修改自己添加的信息、
    admin（超级管理员），超级管理员拥有所有权限、
    tenant（租户）适用于多租户模式下，只能查看、删除、修改本机构添加的信息。</br>
    <br>建议系统自定义角色时，以内置的角色类型开头，比如:admin_功能名称（admin_normal）一般管理员、tenant_normal</br>
    '''
    user = "user" 
    '''普通会员角色'''
    admin = "admin"
    '''超级管理员角色'''
    tenant="tenant"
    '''租户'''

#id类，所有客户端传递过来的实体都要继承此类
class Id(): 
    '''
    统一id，id值在增加的时候不传，修改的时候必须传值
    '''
    id:int
class IdModel(BaseModel,Id): 
    id:int #重新定义 是因为BaseModel不能给继承来的属性赋值
    pass

#数据类型  https:#www.cnblogs.com/cloniu/p/6444065.html  https://docs.sqlalchemy.org/en/14/core/type_basics.html#generic-types
#id实体类，所有表都要继承此类 
class IdEntity(): 
    '''
    id实体类（表）的超级父类，定义统一的字段，所有表都要继承此类
    '''
    __abstract__ = True#https:#segmentfault.com/a/1190000018005342
    id = Column(Integer, primary_key=True, index=True,autoincrement=True)  #primary_key=True 设置为主键
    #添加时间，系统自动生成
    added_time =Column(DateTime, default=datetime.datetime.now)
    # 自动更新 需要设置nullable=False
    updated_time = Column(TIMESTAMP,default=datetime.datetime.now,onupdate=datetime.datetime.now)
    #是否是系统资源标识，系统自动判断赋值，默认为true。true，为系统、行政，即学校或系统，false为个人。为true表示是系统发布的信息资源
    sys =Column(Boolean,default=False)
    #公开，是否公开显示到前台界面，默认true
    open =Column(Boolean,default=True)
    #显示到前台界面，审核是否通过。有些资源需要申请为公开，然后审核通过后才能显示到前台界面,默认为true
    passed =Column(Boolean,default=True)
    #记录排序因子,默认系统不会根据Order排序，客户端前台可以通过查询参数指定排列方式，默认为0
    order= Column(Integer) 
    # 打印形式
    def to_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
    #根据源对象修改目标对象的属性值
    def copy_from(self, source:BaseModel):
        update_temp =source.dict(exclude_unset=True)
        for k, v in update_temp.items():
            if k!="id":
                setattr(self, k, v)
        return self
    '''
    def __repr__(self):#https:#blog.csdn.net/zhouzhiwengang/article/details/112343515  https:#blog.csdn.net/stone0823/article/details/112344065
        """
        Model extension, implementing `__repr__` method which returns all the class attributes
        """
        fields = self.__dict__
        if "_sa_instance_state" in fields:
           del fields["_sa_instance_state"]
        return json.dumps(fields, cls=MyJsonEncoder)  
    '''

class Person(): 
    '''
    人类实体类，抽象类
    '''
    __abstract__ = True#https:#segmentfault.com/a/1190000018005342
    username = Column(String(30), unique=True, index=True)    #用户名
    password = Column(String(60),nullable=False) #加密过的密码
    realname =Column(String(10))#真实姓名
    role =Column(String(50),nullable=False,default=RoleType.user) #角色，可以多个角色。多个之间用,分割角色可以继承
    tel =Column(String(20))#电话
    email = Column(String(32),index=True)
    birthday=Column(DateTime)#出生日期
    qq=Column(String(15))
    address =Column(String(60))#联系地址
    gender =Column(Enum(Gender))#性别
    pic =Column(String(255))#头像
    nation =Column(String(8))#民族
    idcard =Column(String(20))#身份证号码
    zip =Column(String(10))#邮政编码
    areaid =Column(Integer) # 区域id，外键 
    # 区域对象。客户端做增加修改时不需要给此属性赋值（修改对应的areaId即可）  ?可以加
    available_balance =Column(Numeric(precision=18,scale=2)) #可用预存款
    freeze_blance=Column(Numeric(precision=18,scale=2))  #冻结预存款
    Integral =Column(Integer)#积分
    gold =Column(Integer)#金币
    friend_num =Column(Integer)#好友数
    attention_num =Column(Integer)#关注数
    attentioned_num =Column(Integer)#被关注数
    credit =Column(Integer)#会员信用\
    last_login_date =Column(DateTime)#最后登录时间
    login_date =Column(DateTime)#登陆时间
    last_login_ip =Column(String(128))#最后登录IP
    login_ip =Column(String(128))#登录IP
    login_count =Column(Integer)#登录次数
    imei =Column(String(64))#国际移动设备识别码
    emergency_number =Column(String(15))#紧急联系电话
    credentials =Column(String(512))#资格证书

class UserInfo(IdModel):
    '''
    登录用户信息标识类，用户登录后访问资源，携带此用户信息
    '''
    username:str
    password:Optional[str]=None
    role:str="user"

class Token(BaseModel):
    '''
    token类，保存生成的access_token等。refresh_token :刷新token，access_token过期后根据此获取新的token
    '''
    access_token: str
    refresh_token: str
    token_type: str="bearer"

#泛型类
T = TypeVar('T')
class Result(GenericModel, Generic[T]):
    ''' 
    通用返回数据，统一返回结果.code状态码，1为正确，其他为错误。msg提示消息,data返回数据。请调用 success或error定制成功或错误消息
    ''' 
    status: bool=True
    code:int=1 #1表示无错误  其他就是错误码 1001删除失败
    msg:str="" 
    data:Optional[T]=None # 可能连data都没有
    ''' 
    通用返回数据，统一返回结果.code状态码，1为正确，其他为错误。msg提示消息,data返回数据。请调用 success或error定制成功或错误消息
    '''
    '''
    def __init__(self,code=1, msg="",data=None):
        self.code=code
        self.msg=msg
        self.data=data
    '''
    def success(self,msg:Optional[str]=None, data:Optional[T]=None):
        self.code=1
        self.msg=msg
        self.status=True
        self.data=data
        return self
    def error(self,msg:Optional[str]=None,code:Optional[int]=None, data:Optional[T]=None):
        self.code=1001
        self.msg=msg
        self.status=False
        self.data=data
        return self

