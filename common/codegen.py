# 代码生成器
from base64 import encode
from jinja2 import Template
import os
from common.domain import Id
from common.toolkit import *
from typing import Container, Optional, Type
from pydantic import BaseConfig, BaseModel, create_model
from sqlalchemy.inspection import inspect
from sqlalchemy.orm.properties import ColumnProperty
from inspect import isfunction


# https://github.com/tiangolo/pydantic-sqlalchemy/blob/master/pydantic_sqlalchemy/main.py
class OrmConfig(BaseConfig,Id):
    orm_mode = True

def sqlalchemy_to_pydantic(
    db_model: Type, *, config: Type = OrmConfig, exclude: Container[str] = [],prefix:Optional[str]=None,validators:Optional[dict]=None
) -> Type[BaseModel]:
    pre=prefix  if  prefix is not None else ""
    mapper = inspect(db_model)
    fields = {}
    for attr in mapper.attrs:
        if isinstance(attr, ColumnProperty):
            if attr.columns:
                name = attr.key
                if name in exclude:
                    continue
                column = attr.columns[0]
                python_type: Optional[type] = None
                if hasattr(column.type, "impl"):
                    if hasattr(column.type.impl, "python_type"):
                        python_type = column.type.impl.python_type
                elif hasattr(column.type, "python_type"):
                    python_type = column.type.python_type
                assert python_type, f"Could not infer python_type for {column}"
                default = None
                if column.default is None and not column.nullable:
                    default = ...
                elif column.default is not None:
                    default=column.default.arg(None) if(isfunction(column.default.arg)) else column.default.arg
                    #default =column.default.arg  #https://www.weiney.com/2148.html
                fields[name] = (python_type, default)
    pydantic_model = create_model(  #https://blog.csdn.net/swinfans/article/details/89629641  #https://blog.csdn.net/swinfans/article/details/89629641
        pre+db_model.__name__, __config__=config, **fields , __validators__=validators#,__base__=Id,
    )
    return pydantic_model

#控制器路由模板
__tmp_router = """
from importlib.resources import path
from fastapi import APIRouter, Body
from fastapi import FastAPI,Depends,HTTPException, status
from common.basedao import *
from common.domain import Result
from common.query import Pageparam,Pageobj,Whereis
from domain.entities import *
from domain.schemas import *
from common.security import *

#https://www.cnblogs.com/leiziv5/p/15416805.html
'''本文件为codegen自动生成，请谨慎更改，再次自动生成的时候会覆盖此文件内容'''
{% for entity in entities %}
'''
{{entity.title}}路由
'''
{{entity.name|lower}}_router = APIRouter()

@{{entity.name|lower}}_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[List{{entity.name}}]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type={{entity.name}},res_cod="{{entity.name|lower}}_list")), dao= Depends(Dao({{entity.name}}))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**{{entity.name|lower}}_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述：{% if entity.des is defined %} {{entity.des}} {% else %} 无  {% endif %}</br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/{{entity.name|lower}}?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@{{entity.name|lower}}_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[List{{entity.name}}])
async def get(id: int,w=Depends(Whereis(type={{entity.name}},res_cod="{{entity.name|lower}}_get")), dao = Depends(Dao({{entity.name}}))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**{{entity.name|lower}}_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述：{% if entity.des is defined %} {{entity.des}} {% else %} 无  {% endif %}</br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@{{entity.name|lower}}_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type={{entity.name}},res_cod="{{entity.name|lower}}_delete")), dao = Depends(Dao({{entity.name}}))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**{{entity.name|lower}}_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述：{% if entity.des is defined %} {{entity.des}} {% else %} 无  {% endif %}</br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@{{entity.name|lower}}_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: Add{{entity.name}},w=Depends(Whereis(type={{entity.name}},res_cod="{{entity.name|lower}}_add")), dao= Depends(Dao({{entity.name}}))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**{{entity.name|lower}}_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述：{% if entity.des is defined %} {{entity.des}} {% else %} 无  {% endif %}</br>
    - </note>
    '''
    return await dao.add({{entity.name}}(userid=w.user.id).copy_from(o))

@{{entity.name|lower}}_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: Update{{entity.name}},w=Depends(Whereis(type={{entity.name}},res_cod="{{entity.name|lower}}_update")), dao = Depends(Dao({{entity.name}}))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**{{entity.name|lower}}_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述：{% if entity.des is defined %} {{entity.des}} {% else %} 无  {% endif %}</br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)

{% endfor %}

def register(app: FastAPI):
    '''绑定(注册)路由到FastAPI'''
    {% for entity in entities %}
    app.include_router({{entity.name|lower}}_router, prefix="/{{entity.name|lower}}", tags=['{{entity.title}}管理'])
    {% endfor %}
"""
#value值对象模板
__tmp_schema = """
from typing import Optional,List
from pydantic import BaseModel, constr, validator
from common.domain import Id,RoleType
from common.codegen import sqlalchemy_to_pydantic
from domain.entities import *
import re
from common.security import *

#https://blog.csdn.net/qq_33801641/article/details/120320775
#https://blog.csdn.net/qq_33801641/article/details/120320775
'''本文件为codegen自动生成，请谨慎更改，再次自动生成的时候会覆盖此文件内容'''
def is_valid_pswd(v):
    '''验证字符串是否满足 密码格式：必须包含大小写字母及数字,且长度在[6-20]位'''
    if len(v) < 6 or len(v) > 20:
        return False
    if not re.search("[a-z]", v):
        return False
    if not re.search("[A-Z]", v):
        return False
    if not re.search("\d", v):
        return False
    return True

def password_validator(cls, v, values, **kwargs):   
    #encode_password=True if 'encode_password' not in kwargs.keys() else kwargs['encode_password']  #如果不存在这个关键字则表示编码
    '''
    密码验证器：必须包含大小写字母及数字,且长度在[6-20]位
    :param v 待验证的字符串
    :param no_encode_password 是否对字符串进行加密编码返回，登录注册的时候需要返回原始密码，在增加的时候需要进行加密后保存到数据库
    '''
    if not is_valid_pswd(v):
        raise ValueError("密码错误,必须包含大小写字母及数字,且长度在[6-20]位")
    else:
        return v #if no_encode_password  else auth2Helper.encode_password(v)

class LoginUser(BaseModel):
    username: constr(min_length=6) # 无默认值，必填字段   # 有默认值，选填字段  Optional[datetime] = None  # 选填字段
    password: str
   
class RegisterUser(LoginUser):
    email:constr(max_length=30)
    role: str=RoleType.user
    @validator("password")
    def password_rule(cls, v, values, **kwargs):   
         return password_validator(cls,v,values=values,encode_password=False)

class ChangePassword(BaseModel):
    old_password: str
    new_password: str
    @validator('new_password')
    def password_rule(cls, v, values, **kwargs):   
         return password_validator(cls,v,values=values,encode_password=False)

class SendEmail(BaseModel):
    '''
    发送邮件模型
    - <br>to接收人邮件地址</br>
    - <br>subject主题，邮件标题</br>
    - <br>contents 正文内容</br>
    - <br>attachments  附件</br>
    - <br>cc 抄送人</br>
    '''
    to:Union[str,List]
    subject:str
    contents:str
    attachments:Optional[Union[str,List]]=None
    cc:Optional[Union[str,List]]=None

def password_encode(cls, v, values, **kwargs):   
    return authHelper.encode_password(v)

add_user_validators = {
    'password_validator':
    validator('password',check_fields=False)(password_encode)
}

list_excludes=['password'] 
add_excludes=['id','sys','added_time','updated_time','userid'] 
update_excludes=['sys','added_time','updated_time','password','userid'] 
{% for entity in entities %}
List{{entity.name}}= sqlalchemy_to_pydantic({{entity.name}},exclude=list_excludes,prefix='List')
Add{{entity.name}}= sqlalchemy_to_pydantic({{entity.name}},exclude=add_excludes,prefix='Add' {% if entity.validators is defined and entity.validators %},validators={{entity.validators}}{% endif %}) 
Update{{entity.name}}= sqlalchemy_to_pydantic({{entity.name}},exclude=update_excludes,prefix='Update' {% if entity.validators is defined and entity.validators%},validators={{entity.validators}}{% endif %})
{% endfor %}
"""
#c初始化数据模板
__tmp_initdata = """
#  -*-coding:utf8 -*-
import asyncio
from common.basedao import Dao
from domain.entities import *
from common.security import authHelper
from common.domain import *

'''初始化数据'''
'''本文件为codegen自动生成，请谨慎更改，再次自动生成的时候会覆盖此文件内容'''
'''#https://blog.csdn.net/bh57599/article/details/100948050'''
resdao=Dao(Res)
ress=[
Res(name='获取用户信息',value='userinfo'),
{% for entity in entities %}Res(name='{{entity.title}}列表查询',value='{{entity.name|lower}}_list'),Res(name='{{entity.title}}详情查询',value='{{entity.name|lower}}_get'),Res(name='{{entity.title}}增加',value='{{entity.name|lower}}_add'),Res(name='{{entity.title}}修改',value='{{entity.name|lower}}_update'),Res(name='{{entity.title}}删除',value='{{entity.name|lower}}_delete'),{% endfor %}
]
asyncio.run(resdao.add_all(ress))
#初始管理员
userdao=Dao(User)
user=User(username="admin",role=RoleType.admin,password=authHelper.encode_password("123456"))
asyncio.run(userdao.add(user))
"""

def render(tmpl, *args, **kwds):
    '''jinja2 render'''
    vars = dict(*args, **kwds)
    tmp = Template(tmpl)
    return tmp.render(vars).strip()


def save(data, filename='router\codegenerated.py'):
    with open(filename, 'w', encoding="utf-8") as f:
        f.write(data)


def gen(entities, single_file: True = True):
    '''
    生成代码
    :param single_file 是否生成在单个路由文件中，默认为true
    '''
    if not single_file:
        for i, e in enumerate(entities):
            save(data=render(__tmp_router, entities=[e]),
                 filename=f'router\{StrTransverter(e.name).hump2underline()}.py')
    else:
        #生成路由
        code = render(__tmp_router, entities=entities)
        save(code, filename=f'router\\routers.py')
        #生成schema
        code = render(__tmp_schema, entities=entities)
        save(code, filename=f'domain\\schemas.py')
        #s生成初始数据
        code = render(__tmp_initdata, entities=entities)
        save(code, filename=f'db\\initdata.py')
def delete(entities):
    #curPath = os.path.abspath(os.path.dirname(__file__))
    #print( curPath)
    for i, e in enumerate(entities):
        ff = f'D:\\project\\python\\paicloud\\router\{StrTransverter(e).hump2underline()}.py'
        if os.path.exists(ff):  # 如果文件存在
            os.remove(ff)


if __name__ == '__main__':
    entities = [{"name": "Area", "title": "区域"},{"name": "User", "title": "用户","validators":"add_user_validators"}, 
                {"name": "Authority", "title": "权限","des":"角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户"}, 
                {"name": "Res", "title": "资源","des":"角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户"},
                {"name": "AuthorityRes", "title": "权限资源","des":"权限资源中间表，保存权限对应的资源"}, 
                {"name": "Role", "title": "角色","des":"角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户"},
                {"name": "RoleAuthority", "title": "角色权限","des":"角色权限中间表，保存角色对应的权限"}, 
                {"name": "Image", "title": "图片"},
                {"name": "Map", "title": "示意图"},
                {"name": "Trigger", "title": "触发器"},
                {"name": "Record", "title": "历史记录"}
             ]
    #delete(entities)
    gen(entities)
