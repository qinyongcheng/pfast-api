import re
import json
import os
from datetime import datetime
from typing import List, Optional, Union
import decimal
import yagmail
def isdigit(str):
    return re.match("^[-+]?[0-9]+$",str) is not  None
def isfloat(str):
    return re.match("^([-+]?[0-9]+)(\.\d+)$",str) is not  None
def isdate(str):
    return re.match("^\d{4}-\d{1,2}-\d{1,2}$",str) is not  None
def istime(str):
    return re.match("^(20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d$",str) is not  None
def isdatetime(str):
    return re.match("^\d{4}-\d{1,2}-\d{1,2} (20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d$",str) is not  None
    
class StrTransverter(object):#https://blog.csdn.net/m0_37995876/article/details/86680017
    '''字符串转化工具类'''
    # 匹配正则，匹配小写字母和大写字母的分界位置
    p = re.compile(r'([a-z]|\d)([A-Z])')
    p2 = re.compile(r'(_w)')

    def __init__(self,raw_str):
        self.raw_str = raw_str

    def hump2underline(self):
        """
        驼峰形式字符串转成下划线形式
       
        """
        # 这里第二个参数使用了正则分组的后向引用
        sub_str= re.sub(self.p, r'\1_\2',self.raw_str).lower()
        return sub_str

    def underline2hum(self):
        """
        下划线形式字符串转成驼峰形式
         """
        sub_str = self.p2.sub(lambda x:x.group(1)[1].upper(), self.raw_str)
        return  sub_str

class Config():
    '''配置管理'''
    path =  os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '\config.json'
    #path = "config.json"
    #path = r"D:/NinePrice/config.json"
    def __init__(self,path:Optional[str]=None) -> None:
        if path:
            self.path=path

    def init_conf(p: str) -> None:
        content = '{}'
        with open(p, 'w', encoding="UTF-8") as f:
            json.dump(json.loads(content), f, ensure_ascii=False)

    #无key获取所有配置信息
    def get(self,key=None):
        #print("读取配置文件")
        with open(self.path, encoding="UTF-8") as f:
            r = json.load(f)
        if key is None:
            return r
        else:
            return r[str(key)]

    def update(self,key, value):
        #print("设置配置文件")
        content = self.get()
        content[key] = value
        with open(self.path, 'w', encoding="UTF-8") as f:
            json.dump(content, f, ensure_ascii=False)
        return True

    def append_list(self,key, value) -> bool:
        #print("追加配置文件")
        content = self.get()
        li = list(content[key])
        li.append(value)  # 追加
        content[key] = li
        with open(self.path, 'w', encoding="UTF-8") as f:
            json.dump(content, f, ensure_ascii=False)
        return True


    def delete_list(self,key, index: int) -> bool:
        #print("删除列表配置文件")
        content = self.get()
        li = list(content[key])
        del li[index]  # 移除
        content[key] = li
        with open(self.path, 'w', encoding="UTF-8") as f:
            json.dump(content, f, ensure_ascii=False)
        return True


    def update_list(self,key, index: int, value) -> bool:
        print("修改列表配置文件")
        content = self.get()
        li = list(content[key])
        li[index] = value  # 修改
        content[key] = li
        with open(self.path, 'w', encoding="UTF-8") as f:
            json.dump(content, f, ensure_ascii=False)
        return True


    def delete(self,key: str) -> bool:
        print("删除配置文件")
        content = self.get()
        for item in content:
            del item[key]
        with open(self.path, 'w', encoding="UTF-8") as f:
            json.dump(content, f, ensure_ascii=False)
        return True


    def add(self,key: str, value: any) -> bool:
        print("添加配置文件")
        content = self.get()
        content[key] = value
        with open(self.path, 'w', encoding="UTF-8") as f:
            json.dump(content, f, ensure_ascii=False)
        return True

config=Config()

class MyJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime("%Y-%m-%d %H:%M:%S")
        elif  isinstance(obj, decimal.Decimal):
            return float(obj)
        else:
            return json.JSONEncoder.default(self, obj)
        
class Email():
    def __init__(self,user=None,password=None,host=None):#user='xxxx@qq.com', password='xxxxxx', host='smtp.qq.com' port=465
        '''
        :param user 发件人邮箱地址
        :param password 密码 
        :param host 发送邮件的服务器
        :param port 发送邮件的端口
         获取发送邮件服务器参考https://blog.csdn.net/weixin_46521335/article/details/106359984
         https://wenku.baidu.com/view/4361805702f69e3143323968011ca300a6c3f6dc.html
        '''
        if user is None:
            e=config.get("email")
            user=e["user"]
            password=e["password"]
            host=e["host"]
        #链接邮箱服务器
        self.yag = yagmail.SMTP(user=user, password=password, host=host)
    def send(self,to:Union[str,List],subject:str,contents:str,attachments:Optional[Union[str,List]]=None,cc:Optional[Union[str,List]]=None):
        '''
        发送邮件
        :param to 收件人邮箱地址，如果收件人多，可以采用列表，如果多个收件人的话，写成list就行了，如果只是一个账号，就直接写字符串就行to='123@qq.com'
        :param subject 主题，也就是邮件标题
        :param contents 正文
        :param attachments 发送附件，如果附加多可以采用列表[r'd://log.txt', r'd://baidu_img.jpg'].附件如果只有一个的话，用字符串就行，attachments=r'd://log.txt'
        :param cc 抄送人，如果抄送人多个可以采用列表,一个的话直接字符串就可以cc='735@qq.com'
        '''
        self.yag.send(to=to, subject=subject,contents=contents,attachments=attachments ,cc=cc)

'''获取默认配置实例对象'''
if __name__ == '__main__':    
    c=Config()
    #print(c.get("security")["secret"])
    #email=Email()
    #email.send("你的邮箱20@qq.com","爱你","爱你不悔")
