'''
模拟浏览器
Created on 2018年1月16日
@author: Mr.Qin
'''
import requests
from typing import  Union
from random import choice
from requests.packages import urllib3
urllib3.disable_warnings()#设置忽略警告的方式来屏蔽警告
#模拟浏览器
#注：多次请求User-Agent保持一致，否则视为不同浏览器请求
#https://zhuanlan.zhihu.com/p/146913886
#https://blog.csdn.net/duyun0/article/details/119813132
#https://www.jianshu.com/p/8cbf309741e2
#https://blog.csdn.net/qq_37556007/article/details/112450973
class Browser(object):
    def __init__(self,issession:bool=True,baseUrl:str="",cookies:dict={}):
        self.requests=requests.session() if issession else requests
        self.baseUrl=baseUrl
        self._mobile_user_agents = [
            'Mozilla/5.0 (Linux; Android 10; VOG-AL10 Build/HUAWEIVOG-AL10; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/86.0.4240.99 XWEB/3195 MMWEBSDK/20220204 Mobile Safari/537.36 MMWEBID/3668 MicroMessenger/8.0.20.2100(0x28001439) Process/toolsmp WeChat/arm64 Weixin NetType/WIFI Language/zh_CN ABI/arm64',
            'Mozilla/5.0 (Linux; Android 10; PPA-AL20 Build/HUAWEIPPA-AL40; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/86.0.4240.99 XWEB/3195 MMWEBSDK/20220204 Mobile Safari/537.36 MMWEBID/9077 MicroMessenger/8.0.20.2100(0x28001453) Process/appbrand2 WeChat/arm64 Weixin NetType/WIFI Language/zh_CN ABI/arm64 miniProgram',
            'Mozilla/5.0 (Linux; Android 5.1.1; VOG-AL00 Build/LMY49I; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/52.0.2743.100 Mobile Safari/537.36 MMWEBID/2956 MicroMessenger/8.0.2.1860(0x28000234) Process/appbrand0 WeChat/arm32 Weixin NetType/WIFI Language/zh_CN ABI/arm32 miniProgram'
        ]
        # 应该是随机数 作用不大？
        self._x_uuids = ['A34A8770938D5ECADB0464300D58BEFC', 'CC0C7221F77C59A18D4ED4B82B30705D', '6E58714E1CA5C83B4F08279CB792A7AE']
        self.headers = {
            'Connection': 'keep-alive',
            'Accept': 'application/json, text/plain, */*',
            'User-Agent': choice(self._mobile_user_agents),
            'x-uuid': choice(self._x_uuids),
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'zh-CN,en-US;q=0.8',
            #'Content-Type': 'application/json',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty'
        }
        self.cookies = requests.utils.cookiejar_from_dict(cookies,cookiejar = None,overwrite = True)
    def add_cookies(self,new_cookies:Union[dict,str]):
        '''添加cookie，字符串格式的cookie：name=zs;age=18'''
        if isinstance(new_cookies,str):
            new_cookies=self._str_to_dict(new_cookies)
        requests.utils.add_dict_to_cookiejar(self.cookies, new_cookies)

    def add_headers(self,new_headers:Union[dict,str]):
        '''添加header，字符串格式的header：name=zs;age=18'''
        if isinstance(new_headers,str):
            new_headers=self._str_to_dict(new_headers)
        self.headers.update(new_headers)

    def post(self,url,data:dict=None,params:dict=None):
        '''post方式提交'''
        ret=self.requests.post(self.baseUrl+url,data=data,params=params,cookies=self.cookies, headers=self.headers, verify=False);
        self.headers.update({"Referer": ret.url})
        return ret;
    def get(self,url,params:dict=None):
        ret=self.requests.get(self.baseUrl + url, params=params, cookies=self.cookies, headers=self.headers, verify=False,allow_redirects=True)
        self.headers.update({"Referer":ret.url})
        return ret;
    def put(self,url,data:dict=None,params:dict=None):
        ret=self.requests.put(self.baseUrl + url,data=data,params=params,cookies=self.cookies, headers=self.headers, verify=False);
        self.headers.update({"Referer": ret.url})
        return ret;
    def delete(self,url,params:dict=None):
        ret=self.requests.delete(self.baseUrl + url,params=params,cookies=self.cookies, headers=self.headers, verify=False);
        self.headers.update({"Referer": ret.url})
        return ret;
    def _str_to_dict(self,cookie_str):
        newDict = {}
        for item in cookie_str.split(";"):
            strList = item.split("=", 1)
            newDict[strList[0]] = strList[1]    # 设置空字典的key等于切割列表的第一个元素，value等于第二个元素
        return newDict

if __name__ == '__main__':
    b=Browser()
    #b.set_header_cookie("n=1")
    b.add_cookies({"name":"11","age":"2"})
    b.add_cookies("UM_distinctid=1802bd1ca38117-024331d7ec771c-3e604809-1fa400-1802bd1ca393a; Hm_lvt_cf72b87259f0c8e75d3d3476ad8e3e32=1649477524,1649573682,1649932466,1650002611; CNZZDATA1270066=cnzz_eid%3D364061294-1650002650-https%253A%252F%252Festudy.5any.com%252F%26ntime%3D1650002650; CNZZDATA1258577202=978915701-1650002650-https%253A%252F%252Festudy.5any.com%252F%7C1650002650; Hm_lpvt_cf72b87259f0c8e75d3d3476ad8e3e32=1650002652")
    b.add_headers({"m":"22"})
    #print(b._str_to_dict("name=3"))
    #print(type("2"))
    r=b.get("https://estudy.5any.com/webui/ClassroomView/ClassroomIndex.html",params={"n":"2"})
    r = b.get("http://www.py.cn/jishu/jichu/30522.html")
    #print(r.url)