from importlib.resources import path
from fastapi import APIRouter, Body
from fastapi import FastAPI,Depends,HTTPException, status
from common.basedao import *
from common.domain import Result
from common.query import Pageparam,Pageobj,Whereis
from domain.entities import *
from domain.schemas import *
from common.security import *

#https://www.cnblogs.com/leiziv5/p/15416805.html
'''本文件为codegen自动生成，请谨慎更改，再次自动生成的时候会覆盖此文件内容'''

'''
区域路由
'''
area_router = APIRouter()

@area_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListArea]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=Area,res_cod="area_list")), dao= Depends(Dao(Area))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**area_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/area?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@area_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListArea])
async def get(id: int,w=Depends(Whereis(type=Area,res_cod="area_get")), dao = Depends(Dao(Area))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**area_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@area_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=Area,res_cod="area_delete")), dao = Depends(Dao(Area))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**area_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@area_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddArea,w=Depends(Whereis(type=Area,res_cod="area_add")), dao= Depends(Dao(Area))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**area_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.add(Area(userid=w.user.id).copy_from(o))

@area_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateArea,w=Depends(Whereis(type=Area,res_cod="area_update")), dao = Depends(Dao(Area))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**area_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)


'''
用户路由
'''
user_router = APIRouter()

@user_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListUser]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=User,res_cod="user_list")), dao= Depends(Dao(User))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**user_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/user?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@user_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListUser])
async def get(id: int,w=Depends(Whereis(type=User,res_cod="user_get")), dao = Depends(Dao(User))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**user_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@user_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=User,res_cod="user_delete")), dao = Depends(Dao(User))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**user_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@user_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddUser,w=Depends(Whereis(type=User,res_cod="user_add")), dao= Depends(Dao(User))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**user_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.add(User(userid=w.user.id).copy_from(o))

@user_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateUser,w=Depends(Whereis(type=User,res_cod="user_update")), dao = Depends(Dao(User))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**user_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)


'''
权限路由
'''
authority_router = APIRouter()

@authority_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListAuthority]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=Authority,res_cod="authority_list")), dao= Depends(Dao(Authority))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**authority_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/authority?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@authority_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListAuthority])
async def get(id: int,w=Depends(Whereis(type=Authority,res_cod="authority_get")), dao = Depends(Dao(Authority))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**authority_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@authority_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=Authority,res_cod="authority_delete")), dao = Depends(Dao(Authority))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**authority_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@authority_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddAuthority,w=Depends(Whereis(type=Authority,res_cod="authority_add")), dao= Depends(Dao(Authority))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**authority_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    '''
    return await dao.add(Authority(userid=w.user.id).copy_from(o))

@authority_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateAuthority,w=Depends(Whereis(type=Authority,res_cod="authority_update")), dao = Depends(Dao(Authority))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**authority_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)


'''
资源路由
'''
res_router = APIRouter()

@res_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListRes]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=Res,res_cod="res_list")), dao= Depends(Dao(Res))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**res_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/res?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@res_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListRes])
async def get(id: int,w=Depends(Whereis(type=Res,res_cod="res_get")), dao = Depends(Dao(Res))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**res_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@res_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=Res,res_cod="res_delete")), dao = Depends(Dao(Res))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**res_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@res_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddRes,w=Depends(Whereis(type=Res,res_cod="res_add")), dao= Depends(Dao(Res))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**res_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    '''
    return await dao.add(Res(userid=w.user.id).copy_from(o))

@res_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateRes,w=Depends(Whereis(type=Res,res_cod="res_update")), dao = Depends(Dao(Res))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**res_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)


'''
权限资源路由
'''
authorityres_router = APIRouter()

@authorityres_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListAuthorityRes]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=AuthorityRes,res_cod="authorityres_list")), dao= Depends(Dao(AuthorityRes))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**authorityres_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 权限资源中间表，保存权限对应的资源 </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/authorityres?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@authorityres_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListAuthorityRes])
async def get(id: int,w=Depends(Whereis(type=AuthorityRes,res_cod="authorityres_get")), dao = Depends(Dao(AuthorityRes))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**authorityres_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 权限资源中间表，保存权限对应的资源 </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@authorityres_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=AuthorityRes,res_cod="authorityres_delete")), dao = Depends(Dao(AuthorityRes))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**authorityres_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 权限资源中间表，保存权限对应的资源 </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@authorityres_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddAuthorityRes,w=Depends(Whereis(type=AuthorityRes,res_cod="authorityres_add")), dao= Depends(Dao(AuthorityRes))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**authorityres_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 权限资源中间表，保存权限对应的资源 </br>
    - </note>
    '''
    return await dao.add(AuthorityRes(userid=w.user.id).copy_from(o))

@authorityres_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateAuthorityRes,w=Depends(Whereis(type=AuthorityRes,res_cod="authorityres_update")), dao = Depends(Dao(AuthorityRes))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**authorityres_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 权限资源中间表，保存权限对应的资源 </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)


'''
角色路由
'''
role_router = APIRouter()

@role_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListRole]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=Role,res_cod="role_list")), dao= Depends(Dao(Role))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**role_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/role?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@role_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListRole])
async def get(id: int,w=Depends(Whereis(type=Role,res_cod="role_get")), dao = Depends(Dao(Role))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**role_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@role_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=Role,res_cod="role_delete")), dao = Depends(Dao(Role))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**role_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@role_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddRole,w=Depends(Whereis(type=Role,res_cod="role_add")), dao= Depends(Dao(Role))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**role_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    '''
    return await dao.add(Role(userid=w.user.id).copy_from(o))

@role_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateRole,w=Depends(Whereis(type=Role,res_cod="role_update")), dao = Depends(Dao(Role))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**role_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 角色权限资源构成：用户->（拥有）角色,角色->（拥有）权限,权限->（拥有）资源。即若干资源组成权限，若干权限组成角色，一个或多个角色赋予用户 </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)


'''
角色权限路由
'''
roleauthority_router = APIRouter()

@roleauthority_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListRoleAuthority]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=RoleAuthority,res_cod="roleauthority_list")), dao= Depends(Dao(RoleAuthority))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**roleauthority_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 角色权限中间表，保存角色对应的权限 </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/roleauthority?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@roleauthority_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListRoleAuthority])
async def get(id: int,w=Depends(Whereis(type=RoleAuthority,res_cod="roleauthority_get")), dao = Depends(Dao(RoleAuthority))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**roleauthority_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 角色权限中间表，保存角色对应的权限 </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@roleauthority_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=RoleAuthority,res_cod="roleauthority_delete")), dao = Depends(Dao(RoleAuthority))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**roleauthority_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 角色权限中间表，保存角色对应的权限 </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@roleauthority_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddRoleAuthority,w=Depends(Whereis(type=RoleAuthority,res_cod="roleauthority_add")), dao= Depends(Dao(RoleAuthority))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**roleauthority_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 角色权限中间表，保存角色对应的权限 </br>
    - </note>
    '''
    return await dao.add(RoleAuthority(userid=w.user.id).copy_from(o))

@roleauthority_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateRoleAuthority,w=Depends(Whereis(type=RoleAuthority,res_cod="roleauthority_update")), dao = Depends(Dao(RoleAuthority))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**roleauthority_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 角色权限中间表，保存角色对应的权限 </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)


'''
图片路由
'''
image_router = APIRouter()

@image_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListImage]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=Image,res_cod="image_list")), dao= Depends(Dao(Image))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**image_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/image?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@image_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListImage])
async def get(id: int,w=Depends(Whereis(type=Image,res_cod="image_get")), dao = Depends(Dao(Image))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**image_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@image_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=Image,res_cod="image_delete")), dao = Depends(Dao(Image))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**image_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@image_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddImage,w=Depends(Whereis(type=Image,res_cod="image_add")), dao= Depends(Dao(Image))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**image_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.add(Image(userid=w.user.id).copy_from(o))

@image_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateImage,w=Depends(Whereis(type=Image,res_cod="image_update")), dao = Depends(Dao(Image))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**image_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)


'''
示意图路由
'''
map_router = APIRouter()

@map_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListMap]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=Map,res_cod="map_list")), dao= Depends(Dao(Map))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**map_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/map?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@map_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListMap])
async def get(id: int,w=Depends(Whereis(type=Map,res_cod="map_get")), dao = Depends(Dao(Map))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**map_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@map_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=Map,res_cod="map_delete")), dao = Depends(Dao(Map))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**map_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@map_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddMap,w=Depends(Whereis(type=Map,res_cod="map_add")), dao= Depends(Dao(Map))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**map_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.add(Map(userid=w.user.id).copy_from(o))

@map_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateMap,w=Depends(Whereis(type=Map,res_cod="map_update")), dao = Depends(Dao(Map))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**map_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)


'''
触发器路由
'''
trigger_router = APIRouter()

@trigger_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListTrigger]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=Trigger,res_cod="trigger_list")), dao= Depends(Dao(Trigger))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**trigger_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/trigger?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@trigger_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListTrigger])
async def get(id: int,w=Depends(Whereis(type=Trigger,res_cod="trigger_get")), dao = Depends(Dao(Trigger))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**trigger_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@trigger_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=Trigger,res_cod="trigger_delete")), dao = Depends(Dao(Trigger))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**trigger_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@trigger_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddTrigger,w=Depends(Whereis(type=Trigger,res_cod="trigger_add")), dao= Depends(Dao(Trigger))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**trigger_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.add(Trigger(userid=w.user.id).copy_from(o))

@trigger_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateTrigger,w=Depends(Whereis(type=Trigger,res_cod="trigger_update")), dao = Depends(Dao(Trigger))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**trigger_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)


'''
历史记录路由
'''
record_router = APIRouter()

@record_router.get("/", summary="分页查询列表数据", response_model=Result[Pageobj[ListRecord]])
async def list(page: Pageparam = Depends(),w=Depends(Whereis(type=Record,res_cod="record_list")), dao= Depends(Dao(Record))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**record_list**</br>
    - </note>
    ----------
    - *page_no*: 页码
    - *page_size*: 每页记录数
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    - <noet>
    - <br>查询条件格式：api/record?sort=-id&amp;name=武汉&amp;pageNo=1 </br>
    - <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-id根据id字段递减排序；+id根据id字段递增排序。可多个字段排序：如+id,-added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>page_no：查询页码；&lt;=0则查询所有，默认1。</br>
    - <br>page_size：每页显示数量；&lt;=0则查询所有，默认12。</br>
    - <br>条件查询，格式：字段名__操作符=值。如"parentid__eq=1"、 parentid=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
    - <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
    - </note>
    '''
    return await dao.select_page(page_param=page,where=w)

@record_router.get("/{id}",summary="根据id查询实体对象", response_model=Result[ListRecord])
async def get(id: int,w=Depends(Whereis(type=Record,res_cod="record_get")), dao = Depends(Dao(Record))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**record_get**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.select_one(id,where=w)

@record_router.delete("/{id}", summary="根据id删除实体对象", response_model=Result)
async def delete(id: int,w=Depends(Whereis(type=Record,res_cod="record_delete")), dao = Depends(Dao(Record))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**record_delete**</br>
    - </note>
    ----------
    - *id*: id值
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.delete(id,where=w)

@record_router.post("/",summary="添加实体对象", response_model=Result)
async def add(o: AddRecord,w=Depends(Whereis(type=Record,res_cod="record_add")), dao= Depends(Dao(Record))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**record_add**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    '''
    return await dao.add(Record(userid=w.user.id).copy_from(o))

@record_router.put("/", summary="修改实体对象", response_model=Result)
async def update(o: UpdateRecord,w=Depends(Whereis(type=Record,res_cod="record_update")), dao = Depends(Dao(Record))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**record_update**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 无  </br>
    - </note>
    ----------
    '''
    return await dao.update(o,where=w)



def register(app: FastAPI):
    '''绑定(注册)路由到FastAPI'''
    
    app.include_router(area_router, prefix="/area", tags=['区域管理'])
    
    app.include_router(user_router, prefix="/user", tags=['用户管理'])
    
    app.include_router(authority_router, prefix="/authority", tags=['权限管理'])
    
    app.include_router(res_router, prefix="/res", tags=['资源管理'])
    
    app.include_router(authorityres_router, prefix="/authorityres", tags=['权限资源管理'])
    
    app.include_router(role_router, prefix="/role", tags=['角色管理'])
    
    app.include_router(roleauthority_router, prefix="/roleauthority", tags=['角色权限管理'])
    
    app.include_router(image_router, prefix="/image", tags=['图片管理'])
    
    app.include_router(map_router, prefix="/map", tags=['示意图管理'])
    
    app.include_router(trigger_router, prefix="/trigger", tags=['触发器管理'])
    
    app.include_router(record_router, prefix="/record", tags=['历史记录管理'])