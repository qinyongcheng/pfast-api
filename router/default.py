from cgitb import reset
from fastapi import APIRouter, Body
from fastapi import Depends, HTTPException, status
from common.basedao import *
from common.domain import Result,Token
from domain.entities import User
from domain.schemas import *
from common.security import *
from common.toolkit import Email
# https://www.cnblogs.com/leiziv5/p/15416805.html
# https://www.jianshu.com/p/2c2a1c2ee7e0
# https://www.cnblogs.com/leiziv5/p/15416936.html
router = APIRouter()


@router.post("/reg", summary="注册", response_model=Result)
async def register(o:RegisterUser, dao= Depends(Dao(User))):
    ret = await dao.select_one(conditions="username=:user or email=:email", condition_params={
        "user": o.username, "email": o.email})
    if ret.status:
        return Result().error(msg="用户名或邮箱已经存在")
    else:
        o.password = authHelper.encode_password(o.password)
        u = User().copy_from(o)
        return await dao.add(u)


@router.post("/token", summary="表单登录获取token", response_model=Token)
async def access_token(form: AuthForm = Depends(), dao= Depends(Dao(User))):
    '''
    仅仅用于fastapi文档测试登录，如果是客户端如APP、小程序、web端建议用JSON登录方式，以便统一管理返回数据格式
    username 用户名
    password 密码
    '''
    ret = await dao.select_one(conditions="username=:user", condition_params={"user": form.username})
    if not ret.status or not authHelper.verify_password(form.password, ret.data.password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password"+ret.msg,
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = authHelper.encode_token(UserInfo(id=ret.data.id,username=ret.data.username,role=ret.data.role))
    refresh_token = authHelper.encode_refresh_token(UserInfo(id=ret.data.id,username=ret.data.username,role=ret.data.role))
    # {"access_token": access_token, "token_type": "bearer"}
    return Token(access_token=access_token, refresh_token=refresh_token)

@router.post("/login", summary="JSON登录获取token", response_model=Result)
async def login(user:LoginUser, dao = Depends(Dao(User))):
    '''
    username 用户名
    password 密码
    '''
    ret = await dao.select_one(conditions="username=:user", condition_params={"user": user.username})
    if not ret.status or not authHelper.verify_password(user.password, ret.data.password):
        return Result().error(msg="用户名或密码错误,请重新输入")
    access_token = authHelper.encode_token(UserInfo(id=ret.data.id,username=ret.data.username,role=ret.data.role))
    refresh_token = authHelper.encode_refresh_token(UserInfo(id=ret.data.id,username=ret.data.username,role=ret.data.role))
    # {"access_token": access_token, "token_type": "bearer"}
    return Result().success(msg="登录成功",data=Token(access_token=access_token, refresh_token=refresh_token))


@router.get('/refresh_token', summary="刷新token",response_model=Token)
async def refresh_token(refresh_token:str):
    '''
    刷新token，token过期后通过refresh_token获取新的token
    '''
    new_token=authHelper.refresh_token(refresh_token=refresh_token)
    return Token(access_token=new_token, refresh_token=refresh_token)

@router.get('/myinfo', summary="用户获取自己信息",response_model=Result[ListUser],  response_model_exclude=(""))
async def userinfo(user=Depends(Authorize(role="user")), dao = Depends(Dao(User))):
    '''
    - <noet>
    - <br>**本接口需要角色**：**user**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述：用户获取自己信息  </br>
    - </note>
    ----------
    '''
    return await dao.select_one(user.id)


@router.post('/change_mypassword', summary="修改自己密码",response_model=Result)
async def change_password(o:ChangePassword,user=Depends(Authorize(role="user")), dao = Depends(Dao(User))):
    '''
    - <noet>
    - <br>**本接口需要角色**：**user**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 用户修改自己的密码，在原始密码记得的情况下，输入原始密码  </br>
    - </note>
    ----------
    '''
    ret=await dao.select_one(user.id)
    if not ret.status or not authHelper.verify_password(o.old_password, ret.data.password):
        return Result().error("密码修改失败,请输入正确的原始密码")
    u=ret.data
    u.password = authHelper.encode_password(o.new_password)
    return await dao.update(u)

@router.post('/change_otherpassword', summary="管理员重置用户密码",response_model=Result)
async def change_password(userid:int=Body(...,embed=True),password:str=Body(...,embed=True),user=Depends(Authorize(res_cod="change_otherpassword")), dao = Depends(Dao(User))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**change_otherpassword**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 管理员重置别人的密码</br>
    - </note>
    ----------
    '''
    ret=await dao.select_one(userid)
    if not ret.status:
        return Result().error(f"密码修改失败,未找到对应{userid}用户")
    u=ret.data
    u.password = authHelper.encode_password(password)
    return await dao.update(u)

@router.post('/send_email', summary="发送邮件",response_model=Result)
async def send_email(email:SendEmail,user=Depends(Authorize(res_cod="send_email")), dao = Depends(Dao(User))):
    '''
    - <noet>
    - <br>**本接口权限资源值value**：**send_email**</br>
    - </note>
    ----------
    - <noet>
    - <br>详细描述： 发送邮件</br>
    - </note>
    ----------
    '''
    return Email().send(to=email.to,subject=email.subject,contents=email.contents,attachments=email.attachments,cc=email.cc)