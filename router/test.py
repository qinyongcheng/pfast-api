from datetime import timedelta
from email.policy import default
from tkinter import W
from typing import List

from certifi import where
from common.security import AuthForm,authHelper
from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session
from common.basedao import *
from common.domain import *
from domain.entities import User
from common.security import *
from common.query import *
from domain.schemas import *
router = APIRouter()


'''
async def test_error(name: str):
    try:
        print("第1步")
         # finally 拋出异常
        raise HTTPException(status_code=400, detail="姓名錯誤")
        # 返回 name
        yield name
    except Exception  as e:
        print("第3步")
        yield "n"
    finally:
        print("第4步")
       


@router.get("/t")
async def read_items(name: str = Depends(test_error)):

    print("第2步")
    print(name)
    return {"name": name}

@router.get("/tes",dependencies=[Depends(Authorize("read"))])
async def read_items2():
    
    测试

    print("测试")
    
 return {"name"}
'''



'''
@router.get('/test')
async def test(user = Depends(Authorize("test"))):
    return {'hello：': user}
'''
'''
@app.get("/items/", dependencies=[Depends(verify_token), Depends(verify_key)])
async def read_items():
    return [{"item": "Foo"}, {"item": "Bar"}]
'''
@router.get('/test', response_model=Result[List[ListUser]])
async def test(w = Depends(Whereis(type=User)),dao= Depends(Dao(User))):
    print(dao.select_all(where=w))
    return dao.select_all(where=w)

if __name__ == '__main__':
    import json 
    from domain.schemas import *
    o=UserInfo(id=2,username="张三")
    print(json.dumps(o.dict()))
    print (json.loads(json.dumps(o.dict())))

    f=json.loads(json.dumps(o.dict()))

    g=UserInfo(**f)
    print(g.username)
