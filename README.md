# PFastApi

#### 介绍
基于FastApi的快速开发api接口的应用脚手架，高度规范，一键生成

#### 软件架构
1.  common/ 存放程序中常用的自定义模块
    basedao:通用舒服访问库、codegen:代码生成器库、query:数据查询相关库、security:安全（认证/授权）库、toolkit:常用工具库
2.  db/ 存放数据库、数据库初始数据等相关文件
    initdata:初始数据文件（此文件由codegen生成）
3.  domain/ 领域层，包括实体和schemas
    entities:实体库、schemas（此文件由codegen生成）
4.  router/ 路由
    default:默认路由，集成系统常用功能、routers:应用路由，包括实体常用的增删改查（此文件由codegen生成）
5.  service/ 服务层 
4.  run.py 程序的启动文件，一般放在项目的根目录下，因为在运行时会默认将运行文件所在的文件夹sys.path的第一个路径，这样就省去了处理环境变量的步骤
5.  setup.py 安装、部署、打包的脚本。
6.  requirements.txt 存放软件依赖的外部Python包列表


#### 安装教程

1.  克隆下载

#### 使用说明

#### 技术交流群
QQ：240131047

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
